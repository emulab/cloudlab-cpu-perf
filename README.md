Evalution of CPU Performance of CloudLab Servers
===========

This repository contains data and analysis code for testbed-wide evaluation of CPU performance, its variability and anomalies.

Related repositories:

- `https://gitlab.flux.utah.edu/emulab/cloudlab-benchmarks` -- scripts that collect benchmarking results on CloudLab 
- `https://gitlab.flux.utah.edu/emulab/cloudlab-orchestration` -- orchestration scripts

The paper "Taming Performance Variability" from USENIX OSDI'18, describing the study that laid the foundation for this CPU analysis,
is available at: `https://www.flux.utah.edu/paper/maricq-osdi18`.

===========

Use tag `MERIT19` to access the code and the data corresponding to the study submitted to the MERIT 2019 workshop (https://icnp19.cs.ucr.edu/merit.html)

===========

With comments and questions related to this data and analysis, please contact: Dmitry Duplyakin <dmdu@cs.flux.utah.edu>
